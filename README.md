# Frontend en VUEjs para prueba técnica SOHO

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
Asegurarse de que la ruta de consumo al endpoint REST coincida con el nombre del proyecto Laravel. La ruta se encuentra en el componente "Producto.vue"

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
